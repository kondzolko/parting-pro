require 'json'

class Organization
  attr_reader :client, :json

  def initialize(org_name = nil, json = nil)
    @client = Client.new
    @hash = json || client.get("https://api.github.com/users/#{org_name}")
  end

  def self.from_org_name(username)
    new(username)
  end

  def self.from_json(json)
    new(nil, json)
  end
end
