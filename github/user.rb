class User
  attr_reader :client, :json, :username

  def initialize(username = nil, json = nil)
    @username = username
    @client = Client.new
    @json = json || client.get("https://api.github.com/users/#{username}")
  end

  def self.from_username(username)
    new(username)
  end

  def self.from_json(json)
    new(json['login'], json)
  end

  def repos
    resp = client.get(json['repos_url'])
    resp.map do |json|
      Repo.from_json(json)
    end
  end

  def followers
    resp = client.get(json['followers_url'])
    resp.map do |json|
      User.from_json(json)
    end
  end

  def organizations
    resp = client.get(json['organizations_url'])
    resp.map do |json|
      Organization.from_json(json)
    end
  end
end
