require 'http'
require 'json'

class Client
  attr_accessor :http

  def initialize()
    @http = HTTP.auth("token #{ENV['TOKEN']}")
  end

  def get(endpoint)
    resp = http.get(endpoint)

    if resp.code == 200
      JSON.parse(http.get(endpoint).to_s)
    else
      raise resp
    end
  end
end
