# def count_duplicates(str)
#   str_arry = str.downcase.split('').uniq
#   str = str.downcase.split('')
#   arr = []
#
#   str_arry.each do |e|
#     arr << 1 if str.count(e.to_s) > 1
#   end
#   puts arr.sum
# end


# def count_duplicates(str)
#   str = str.downcase.chars
#   res = str.uniq.map { |c| c if str.count(c) > 1 }
#   res.compact.count
# end


# def count_duplicates(str)
#   str.downcase.chars.group_by(&:itself).count { |k, v| v.size > 1 }
# end


def count_duplicates(str)
  str.downcase.chars.uniq.count { |char| str.downcase.count(char) > 1 }
end

count_duplicates('abcde')
count_duplicates('aabbcde')
count_duplicates('aabBcde')
count_duplicates('indivisibility')
count_duplicates('Indivisibilities')
count_duplicates('aA11')
count_duplicates('ABBA')